//
//  SatCell.swift
//  nycschools
//
//  Created by Ismael Zavala on 5/18/22.
//

import UIKit
import CoreData

// Cell that will be used to show the score details
class SatCell: SchoolDetailsCell {
    
    @IBOutlet weak var numberOfStudentsLabel: UILabel!
    @IBOutlet weak var criticalReadingLabel: UILabel!
    @IBOutlet weak var mathLabel: UILabel!
    @IBOutlet weak var writingLabel: UILabel!
    
    override func configure(school: School?, score: Score?) {
        numberOfStudentsLabel.text = "Number of Test Takers: \(score?.numberOfStudents ?? "")"
        criticalReadingLabel.text = "Critical Reading Average Score: \(score?.readingScore ?? "")"
        mathLabel.text = "Math Average Score: \(score?.mathScore ?? "")"
        writingLabel.text = "Writing Average Score: \(score?.writingScore ?? "")"
    }
}
