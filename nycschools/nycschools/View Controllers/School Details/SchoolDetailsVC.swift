//
//  SchoolDetailsVC.swift
//  nycschools
//
//  Created by Ismael Zavala on 5/18/22.
//

import UIKit
import CoreData

/// View that will display the details of the selected school.
class SchoolDetailsVC: UIViewController {
    
    // MARK: - IB outlets
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Class properties
    
    // Object id of the selected school.
    var schoolId: NSManagedObjectID?
    
    // Helper class to keep track of score data.
    var scoreHelper = ScoresHelper()
    
    // MARK: - Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setting tableview
        tableView.delegate = self
        tableView.dataSource = self
        
        // Registering all cell types.
        for cell in SchoolDetailsCell.SchoolDetailCells.allCases {
            tableView.register(UINib(nibName: cell.getCellIdentifier(), bundle: nil),
                               forCellReuseIdentifier: cell.getCellIdentifier())
        }
        
        // Fetching latest scores data if needed
        scoreHelper.delegate = self
        scoreHelper.fetchLatestScoresIfNeeded(currentSchoolObjectId: schoolId)
    }
    
    // MARK: - Dirrections UI
    
    /// Displays Directions action sheet with Maps, Google Maps and Cancel actions.
    func displayDirectionsActionSheet() {
        // Initialize action sheet.
        let directionsActionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        // Get school coordinates
        let (latitude, longitude) = scoreHelper.getSchoolCoordinates()

        // Create Apple Maps action.
        let appleMapsAction = UIAlertAction(title: "Maps", style: .default) { _ in
            
            DirectionsHelper.getDirections(destinationLatitude: latitude ?? "",
                                                     destinationLongitude: longitude ?? "",
                                                     withApp: .AppleMaps)
        }
        
        // Create Google Maps action.
        let googleMapsAction = UIAlertAction(title: "Google Maps", style: .default) { _ in
            
            DirectionsHelper.getDirections(destinationLatitude: latitude ?? "",
                                                     destinationLongitude: longitude ?? "",
                                                     withApp: .GoogleMaps)
        }

        // Create cancel action.
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)

        // Add actions.
        directionsActionSheet.addAction(appleMapsAction)
        directionsActionSheet.addAction(googleMapsAction)
        directionsActionSheet.addAction(cancelAction)

        self.present(directionsActionSheet, animated: true, completion: nil)
    }
}

// MARK: - Tableview Methods
extension SchoolDetailsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Getting the type of cell based on row number.
        guard let cellType = SchoolDetailsCell.SchoolDetailCells(rawValue: indexPath.row) else {
            return UITableViewCell()
        }
        
        // Initializing cell
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellType.getCellIdentifier() ) as? SchoolDetailsCell else {
            return UITableViewCell()
        }
        
        // Verifying there's a valid id, and valid school based on id
        if let schoolObjectId = schoolId, let school = SchoolHelper().getSchool(objectId: schoolObjectId) {
            // Configuring cell based on name and value if school is available
            let (cellName, cellValue) = cellType.getCellValues(school: school)
            cell.configure(name: cellName, value: cellValue)
        }
        
        // Configuring cell based on school id and test ID
        cell.configure(school: scoreHelper.currentSchool, score: scoreHelper.currentScore)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Returning number of all possible cell types.
        return SchoolDetailsCell.SchoolDetailCells.allCases.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        // Getting the type of cell based on row number.
        guard let cellType = SchoolDetailsCell.SchoolDetailCells(rawValue: indexPath.row) else {
            return 0
        }

        // Returning section size.
        return cellType.getCellSize()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Getting the type of cell based on row number.
        guard let cellType = SchoolDetailsCell.SchoolDetailCells(rawValue: indexPath.row) else {
            return
        }
        
        if cellType == .location {
            displayDirectionsActionSheet()
        }
    }
}

// MARK: - ScoresHelperDelegate
extension SchoolDetailsVC: ScoresHelperDelegate {
    // Called when all data is available, refreshing tableview.
    func fetchedLatestScores() {
        tableView.reloadData()
    }
}
