//
//  SchoolHelperTests.swift
//  nycschoolsTests
//
//  Created by Ismael Zavala on 5/19/22.
//

import XCTest
import OHHTTPStubs
@testable import nycschools

/// Test for the school helper class.
class SchoolHelperTests: XCTestCase, SchoolHelperDelegate {
    
    // Helper class
    var schoolHelper = SchoolHelper()
    
    // Delay used to wait for delegate call response.
    var delay = 15.0
    let didFinish = XCTestExpectation(description: #function)

    override func setUpWithError() throws {
        // Removing stubs to ensure previous stub setup doesn't interfere with these tests
        HTTPStubs.removeAllStubs()
        
        // Ensuring core data is clear of data.
        let schoolDataLayer = DataLayer<School>()
        schoolDataLayer.deleteAll()
        
        // Seting up stubs to ensure successful api call using test json
        schoolHelper = SchoolHelper()
        schoolHelper.delegate = self
    }

    override func tearDownWithError() throws {
        // Removing stubs to ensure stub setup in this test class doesn't interfere with other tests
        HTTPStubs.removeAllStubs()
    }
    
    /// Testing the parsing and fetching of schools within the test json.
    func testFetchingSchools() {
        // Setting stub for succesful 200 status code
        setupStub(statusCode: 200)
        
        // Setting test class as delegate to test results after completion
        schoolHelper.delegate = self
        
        // Fetching schools from test json
        schoolHelper.fetchLatestSchools()
        
        wait(for: [didFinish], timeout: delay)
    }
    
    /// Delegate call fired when schools have been fetched.
    func fetchedLatestSchools() {
        // Checking that there were enough schools created
        XCTAssertTrue(schoolHelper.getSchoolCount() > 400)
        
        didFinish.fulfill()
    }

    /// Testing ability to fetch school using object ID
    func testGettingSchool() {
        let schoolHelper = SchoolHelper()
        let testSchool = createSampleSchool()
        
        XCTAssertEqual(schoolHelper.getSchool(objectId: testSchool.objectID), testSchool)
    }
    
    // MARK: - Sample Core Data
    // Creating test school for testing purposes
    func createSampleSchool() -> School {
        let schoolDataLayer = DataLayer<School>()
        
        let testSchool = schoolDataLayer.create()
        testSchool.school_name = "Testing"
        testSchool.neighborhood = "Test Neighborhood"
        testSchool.phone_number = "123"
        
        return testSchool
    }
    
    // MARK: - stub
    private func setupStub(statusCode: Int32) {
        let filePath = "schools.json"
        stub(condition: isHost("data.cityofnewyork"), response: { _ in
            guard let fileAtPath = OHPathForFile(filePath, type(of: self)) else { return HTTPStubsResponse() }

            return HTTPStubsResponse(
                fileAtPath: fileAtPath,
                statusCode: statusCode,
                headers: ["Content-Type": "application/json"]
            )
        })
    }
}
