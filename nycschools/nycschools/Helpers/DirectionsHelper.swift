//
//  DirectionsHelper.swift
//  nycschools
//
//  Created by Ismael Zavala on 5/19/22.
//

import UIKit
import CoreLocation

/// Holds the possible apps to open directions.
enum DirectionsAppOptions {
    case AppleMaps, GoogleMaps
}

/// Handles preparation of URLs for navigation using coordinates, prompting Apple or Google Maps and error scenarios.
class DirectionsHelper {
    
    /// Uses passed destination coordinates to open the desired application with directions from the user's current location coordinate.
    /// - Parameters:
    ///   - destinationLatitude: Destination latitue value.
    ///   - destinationLongitude: Destiation longitude value.
    ///   - withApp: App which will open for navigation.
    ///   - completion: Whether iOS was able to open the application.
    static func getDirections(destinationLatitude: String, destinationLongitude: String, withApp: DirectionsAppOptions) {
        
        let manager = CLLocationManager()
        let userLatitude = manager.location?.coordinate.latitude
        let userLongitude = manager.location?.coordinate.longitude
        
        /// Final URL pointing to the desired app for navigation.
        var url: URL?
        
        // Prepare the URL for each Turn By Turn option.
        switch withApp {
        case .AppleMaps:
            guard let appleMapsUrl = URL(string: "http://maps.apple.com/?saddr=\(String(describing: userLatitude)),\(String(describing: userLongitude))&daddr=\(destinationLatitude),\(destinationLongitude)") else {
                print("Unable to create URL object for Apple Maps.")
                return
            }
            
            url = appleMapsUrl
            
        case .GoogleMaps:
            
            guard let googleMapsUrl = URL(string: "comgooglemaps://?saddr=\(String(describing: userLatitude)),\(String(describing: userLongitude))&daddr=\(destinationLatitude),\(destinationLongitude)&directionsmode=traffic") else {
                print("Unable to create URL object for Google Maps.")
                return
            }
            
            url = googleMapsUrl
        }
        
        // Check that the link can be opened.
        guard let unwrappedUrl = url, UIApplication.shared.canOpenURL(unwrappedUrl) else {
            print("Currently unable to open Apple Maps or Google Maps on this device.")
            return
        }
        
        // Open link.
        UIApplication.shared.open(unwrappedUrl, options: [:], completionHandler: nil)
    }
}

