# Introduction

This is a Sample application to demonstrate my coding abilities by fetching NYC schools and their SAT scores.

# Reference links

- [GitLab URL](https://gitlab.com/ismael9291/20220516-ismaelzavala-nycschools/-/tree/master)
- - Clone URL 
https://gitlab.com/ismael9291/20220516-ismaelzavala-nycschools.git

# Getting started

All of the cocoapods are within the repository following the directions of the sample project. 
If issues arrise, please feel free to contact me at ismael9291@gmail.com

## What's contained in this project

- A data layer using core data to parse and save the json results received.
- A service layer that combines functionality of both the school and scores service calls by using a URLSession/
- Helper classes to manipulate and fetch data for schools and scores. 
- Custom views in the form of tableview cells that will be used throughout the app. 
- Coordinators to encapsulate flow navigation. 
- View controllers to display the main UI. 
- Additional Files to store any project wide variables or extensions. 
- Unit tests that cover view controllers, custom views, service calls and data layer testing that equal to 60% code coverage accross the app.

## Given more time...
Unfortunately I did not have more time to implement more features but I can definitely talk about how to implement them at a future time. 
- Log helper that adds logs to a textfile instead of printing to the console and providing a way to share logs to developers to better identifiy potential bugs. 
- Better UI to showcase errors across the app, I added print statements but given more time i would add UI that would be displayed to the user to tell them that an error happened. 
- Crashlytics integration with their logging to showcase importance of crash events and providing extra information when those crashes happen for better developer suppport. 
