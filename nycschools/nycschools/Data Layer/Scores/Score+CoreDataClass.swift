//
//  Score+CoreDataClass.swift
//  nycschools
//
//  Created by Ismael Zavala on 5/17/22.
//
//

import Foundation
import CoreData

/// Core data class in charge of holding information for each NYC School
@objc(Score)
public class Score: NSManagedObject, Codable {
    // Coding keys used to parse json into core data object
    enum ScoreCodingKeys: String, CodingKey {
        case dbn = "dbn"
        case schoolName = "school_name"
        case numberOfStudents = "num_of_sat_test_takers"
        case readingScore = "sat_critical_reading_avg_score"
        case mathScore = "sat_math_avg_score"
        case writingScore = "sat_writing_avg_score"
    }
    
    // Decoding data into score properties
    required convenience public init(from decoder: Decoder) throws {
        
        // Make a managed object context available within init(from:) so we can properly initialize the managed object.
        guard let context = CoreDataManager.sharedManager.persistentContainer?.viewContext else {
            throw NSError()
        }
        
        self.init(context: context)
        
        // Initializing container based on keys that will be used to decode
        let container = try decoder.container(keyedBy: ScoreCodingKeys.self)
        
        do {
            // Attempting to decode individual properties
            self.dbn = try container.decodeIfPresent(String.self, forKey: .dbn)
            self.school_name = try container.decodeIfPresent(String.self, forKey: .schoolName)
            self.numberOfStudents = try container.decodeIfPresent(String.self, forKey: .numberOfStudents)
            
            self.readingScore = try container.decodeIfPresent(String.self, forKey: .readingScore)
            self.mathScore = try container.decodeIfPresent(String.self, forKey: .mathScore)
            self.writingScore = try container.decodeIfPresent(String.self, forKey: .writingScore)
            
        } catch let error as NSError {
            print("Error: \(error). Error description: \(error.debugDescription)")
        }
    }
    
    /// Required for Encodable support
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: ScoreCodingKeys.self)
        try container.encode(dbn, forKey: .dbn)
        try container.encode(school_name, forKey: .schoolName)
        try container.encode(numberOfStudents, forKey: .dbn)
        
        try container.encode(readingScore, forKey: .readingScore)
        try container.encode(mathScore, forKey: .mathScore)
        try container.encode(writingScore, forKey: .writingScore)
    }
}
