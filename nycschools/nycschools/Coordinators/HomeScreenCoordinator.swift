//
//  HomeScreenCoordinator.swift
//  nycschools
//
//  Created by Ismael Zavala on 5/16/22.
//

import UIKit

/// Coordinator in charge of displaying the home screen view controller
class HomeScreenCoordinator: Coordinator {
    weak var rootNav: UINavigationController?
    var rootView: HomeScreenVC?
    
    // Initializing view controller
    init(_ nav: UINavigationController) {
        rootView = HomeScreenVC(nibName: "HomeScreenVC", bundle: nil)
        rootView?.title = "NYC Schools"
        rootNav = nav
    }
    
    // Adding view controller to navigation stack
    func start() {
        guard let rootView = rootView else { return }
        
        rootNav?.pushViewController(rootView, animated: false)
        self.rootView = nil
    }
}
