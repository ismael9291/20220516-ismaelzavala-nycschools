//
//  SchoolDetailsCell.swift
//  nycschools
//
//  Created by Ismael Zavala on 5/18/22.
//

import UIKit
import CoreData

/// Superclass for school details cells.
class SchoolDetailsCell: UITableViewCell {
    func configure(school: School?, score: Score?) { }
    func configure(name: String?, value: String?) { }
    
    // Enum containing all of the possible cells to be displayed in schoool details VC
    enum SchoolDetailCells: Int, CaseIterable {
        case satScroes
        case webAddress
        case phoneNumber
        case location
        
        // Returns cell size.
        func getCellSize() -> CGFloat {
            switch self {
            case .satScroes:
                return CGFloat(150)
                
            case .webAddress:
                return CGFloat(50)
                
            case .phoneNumber:
                return CGFloat(50)
                
            case .location:
                return CGFloat(50)
            }
        }
        
        // Returns identifier of cell used.
        func getCellIdentifier() -> String {
            switch self {
            case .satScroes:
                return "SatCell"
                
            case .webAddress, .phoneNumber, .location:
                return "SchoolPropertyCell"
            }
        }
        
        // Returns the data for each cell
        func getCellValues(school: School) -> (String?, String?) {
            switch self {
            case .satScroes:
                return (nil, nil)
                
            case .webAddress:
                return ("Web Address", school.website)
                
            case .phoneNumber:
                return ("Phone Number", school.phone_number)
                
            case .location:
                return ("Location", school.location)
            }
        }
    }
}
