//
//  PredicateExtension.swift
//  nycschools
//
//  Created by Ismael Zavala on 5/17/22.
//

import Foundation

extension NSPredicate {

    static func getStringPredicate(key: String, value: String) -> NSPredicate {
        return NSPredicate(format: "%K CONTAINS %@", key, value)
    }

    static func getIntPredicate(key: String, value: Int32) -> NSPredicate {
        return NSPredicate(format: "%K == %i", key, value)
    }
}
