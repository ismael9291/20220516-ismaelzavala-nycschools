//
//  SchoolDetailsVCTests.swift
//  nycschoolsTests
//
//  Created by Ismael Zavala on 5/19/22.
//

import XCTest
@testable import nycschools

/// Test for the school detail screen.
class SchoolDetailsVCTests: XCTestCase {
    
    var viewController: SchoolDetailsVC!
    
    override func setUpWithError() throws {
        // Initializing view controller
        viewController = SchoolDetailsVC(nibName: "SchoolDetailsVC", bundle: nil)
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    // Loading view controller
    func startViewController() {
        viewController.loadView()
        viewController.viewDidLoad()
    }
    
    // Testing intial state of view controller
    func testInitialUI() {
        startViewController()
        
        // Ensuring there are 4 cells at launch.
        XCTAssertEqual(viewController.tableView(viewController.tableView, numberOfRowsInSection: 0), 4)
        
        guard let tableView = viewController.tableView else {
            XCTFail("Unable to get tableview.")
            return
        }
        
        // Iterating through all cell types
        for cellType in SchoolDetailsCell.SchoolDetailCells.allCases {
            
            // Setting indexpath based on cell type value.
            let indexPath = IndexPath(item: cellType.rawValue, section: 0)
            
            // Getting current cell height
            guard let cellHeight = tableView.delegate?.tableView?(tableView, heightForRowAt: indexPath) else {
                XCTFail("Unable to get tableview row height")
                return
            }
            
            // Verifying clel height is the same as its intended cell size based on cell type
            XCTAssertEqual(cellHeight, cellType.getCellSize())
            
            // Verifying the correct cells are being created based on cell type
            switch cellType {
            case .satScroes:
                guard let _ = viewController.tableView(tableView, cellForRowAt: indexPath) as? SatCell else {
                    XCTFail("Unable to cast into SatCell cells")
                    return
                }
                
            case .webAddress, .phoneNumber, .location:
                guard let _ = viewController.tableView(tableView, cellForRowAt: indexPath) as? SchoolPropertyCell else {
                    XCTFail("Unable to cast into property cells")
                    return
                }
            }
        }
    }
}
