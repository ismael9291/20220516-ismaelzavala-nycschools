//
//  ScoresHelper.swift
//  nycschools
//
//  Created by Ismael Zavala on 5/18/22.
//

import Foundation
import CoreData

/// Protocol that informs when fetching latest test results is complete.
protocol ScoresHelperDelegate: AnyObject {
    func fetchedLatestScores()
}

/// Helper class to keep track of current school and its repective score
class ScoresHelper {
    // Delegate used when all data is ready
    weak var delegate: ScoresHelperDelegate?
    
    // School and score used to pupulate cells
    var currentSchool: School?
    var currentScore: Score?
    
    // Data layer to access core data elements.
    let scoresLayer = DataLayer<Score>()
    
    /// Fetching school from core data and fetching all score if needed.
    /// - Parameter currentSchoolObjectId: Object id of the current school selected.
    func fetchLatestScoresIfNeeded(currentSchoolObjectId: NSManagedObjectID?) {
        
        // Fetching and keeping track of currently selected school
        if let schoolObjectId = currentSchoolObjectId {
            currentSchool = SchoolHelper().getSchool(objectId: schoolObjectId)
        }
        
        // Checking if there are any scores saved in core data
        guard scoresLayer.getAll(sortKey: nil).count == 0 else {
            completeScoreFetch()
            return
        }
        
        // Initializing score service and fetching all scores
        let scoresService = ScoreService()
        scoresService.getAllScores { _ in
            DispatchQueue.main.async {
                self.completeScoreFetch()
            }
        }
    }
    
    /// Getting score to selected school and informing the delegate
    func completeScoreFetch() {
        // Getting the score to the current selected school
        self.getScore()
        // Informing delegate of data readiness
        self.delegate?.fetchedLatestScores()
    }
    
    /// Fetching the score from core data that matches the selected school
    func getScore() {
        
        // Unwrapping selected school
        guard let fetchedSchool = currentSchool else { return }
        
        // Fetching from core data using the dbn
        currentScore = scoresLayer.getAllByParameters(parameters: [(key: "dbn", value: fetchedSchool.dbn ?? "")], sortKey: nil, ascending: false).first
    }
    
    /// Returning school coordinates.
    func getSchoolCoordinates() -> (String?, String?) {
        guard let school = currentSchool else { return (nil, nil) }
        
        return (school.latitude, school.longitude)
    }
}
