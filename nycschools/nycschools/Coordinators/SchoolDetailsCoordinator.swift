//
//  SchoolDetailsCoordinator.swift
//  nycschools
//
//  Created by Ismael Zavala on 5/18/22.
//

import UIKit
import CoreData

/// Coordinator in charge of displaying school details view controller
class SchoolDetailsCoordinator: Coordinator {
    weak var rootNav: UINavigationController?
    var rootView: SchoolDetailsVC?
    
    // Initializing view controller and setting needed properties
    init(_ nav: UINavigationController, _ selectedSchoolId: NSManagedObjectID, schoolName: String?) {
        rootView = SchoolDetailsVC(nibName: "SchoolDetailsVC", bundle: nil)
        rootView?.schoolId = selectedSchoolId
        rootView?.title = schoolName
        rootNav = nav
    }
    
    /// Adding view controller to navigation stack
    func start() {
        guard let rootView = rootView else { return }
        
        rootNav?.pushViewController(rootView, animated: false)
        self.rootView = nil
    }
}

