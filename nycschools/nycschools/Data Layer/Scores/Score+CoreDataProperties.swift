//
//  Score+CoreDataProperties.swift
//  nycschools
//
//  Created by Ismael Zavala on 5/17/22.
//
//

import Foundation
import CoreData


extension Score {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Score> {
        return NSFetchRequest<Score>(entityName: "Score")
    }

    @NSManaged public var dbn: String?
    @NSManaged public var school_name: String?
    @NSManaged public var numberOfStudents: String?
    
    @NSManaged public var readingScore: String?
    @NSManaged public var mathScore: String?
    @NSManaged public var writingScore: String?

}

extension Score : Identifiable {

}
