//
//  HomeScreenVC.swift
//  nycschools
//
//  Created by Ismael Zavala on 5/16/22.
//

import UIKit

/// Class that holds initial screen to show list of schools.
class HomeScreenVC: UIViewController {
    
    // MARK: - IB Outlets
    
    // Tableview holding cells for schools.
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // MARK: - Class properties
    
    // Helper class in charge of handling school data.
    let schoolHelper = SchoolHelper()
    
    // Refresh control to reload latest results
    private let refreshControl = UIRefreshControl()
    
    // MARK: - Lifecycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setting up search bar
        searchBar.delegate = self
        
        // Setting up tableview.
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: Constants.CustomViewNames.schoolCellName, bundle: nil),
                           forCellReuseIdentifier: Constants.CustomViewNames.schoolCellName)

        // Setting up helper class to fetch schools.
        schoolHelper.delegate = self
        schoolHelper.fetchLatestSchoolsIfNeeded()
        
        // Initializing refresh UI
        setUpRefreshControl()
    }
    
    // MARK: - UI Setup
    /// Initialization of refresh control.
    private func setUpRefreshControl() {
        refreshControl.addTarget(self, action: #selector(refreshHomeData(_:)), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }
    
    /// Triggered when user performs a pull to refresh.
    @objc func refreshHomeData(_ sender: Any) {
        schoolHelper.fetchLatestSchools()
    }
}

// MARK: - Tableview Methods
extension HomeScreenVC: UITableViewDelegate, UITableViewDataSource {
    /// Setting how many cells the tableview will hold.
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Returning the total cell count within our data layer.
        return schoolHelper.getSchoolCount()
    }
    
    /// Initializing cell to be displayed in the tableview
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CustomViewNames.schoolCellName) as? SchoolCell else {
            return UITableViewCell()
        }
        
        // Fetching school based on index of cell
        guard let school = schoolHelper.getSchool(index: indexPath.row) else {
            return UITableViewCell()
        }
        
        // Setting up cell based on school object
        cell.configure(objectId: school.objectID)
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // Getting nav controller and school selected
        guard let school = schoolHelper.getSchool(index: indexPath.row),
        let rootNav = self.navigationController else {
            print("Unable to get navigationController")
            return
        }
        
        // Initializing and starting coordinator to show the school details view controller.
        let schoolDetailsCoordinator = SchoolDetailsCoordinator(rootNav, school.objectID, schoolName: school.school_name)
        schoolDetailsCoordinator.start()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.CustomViewProperties.schoolCellHeight
    }
}

// MARK: - SearchBar Delegate
extension HomeScreenVC: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // Keeping track of search string
        schoolHelper.searchString = searchText
        
        // Reloading tableview to display latest results based on search
        tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        // Dismissing keyboard
        searchBar.resignFirstResponder()
    }
}

// MARK: - SchoolHelperDelegate
extension HomeScreenVC: SchoolHelperDelegate {
    func fetchedLatestSchools() {
        // Stopping refreshing UI from pull to refresh
        refreshControl.endRefreshing()
        
        // Hiding initial activity indicator
        activityIndicator.isHidden = schoolHelper.getSchoolCount() > 0
        
        // Reloading tableview to show latest results
        tableView.reloadData()
    }
}
