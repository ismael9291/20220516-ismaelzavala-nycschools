//
//  DataLayer.swift
//  nycschools
//
//  Created by Ismael Zavala on 5/17/22.
//

import Foundation
import CoreData

/// Class in charge of all core data interactions based on the manged object passed in.
class DataLayer<T: NSManagedObject> {
    // Context of the current core data store thats loaded.
    private var context: NSManagedObjectContext? {
        return CoreDataManager.sharedManager.persistentContainer?.viewContext
    }
    
    /// Saving any changes made to the core data database.
    func save() {
        if let currentContext = context, currentContext.hasChanges {
            do {
                try context?.save()
            } catch let error as NSError {
                print("Saving of \(String(describing: self)) failed. Error: \(error)")
            }
        }
    }
    
    
    /// Creating new core data object
    /// - Returns: Newly created core data object
    func create() -> T {
        return T.init(entity: T.entity(), insertInto: context)
    }
    
    /// Fetching all of the objects that match the managed object
    /// - Parameters:
    ///   - sortKey: The string property that will be used to sort results
    ///   - ascending: Bool determining the ascending order
    /// - Returns: The core data objects that match search criteria
    func getAll(sortKey: String?, ascending: Bool = true) -> [T] {
        // Temp array that will hold results
        var allObjects = [T]()
        // Initializing fetch request
        let fetchRequest = NSFetchRequest<T>(entityName: String(describing: T.self))
        
        // If there's a sort key, adding that to the fetch request
        if let passedSortKey = sortKey {
            let sort = NSSortDescriptor(key: passedSortKey, ascending: ascending)
            fetchRequest.sortDescriptors = [sort]
        }
        
        do {
            // Performing fetch request
            if let currentContext = context {
                allObjects = try currentContext.fetch(fetchRequest)
            }
            
        } catch let error as NSError {
            print("Fetching of \(String(describing: self)) failed. Error: \(error)")
        }
        
        // Returning results of fetch request
        return allObjects
    }
    
    /// Performing fetch request on core data objects based on parameters
    /// - Parameters:
    ///   - parameters: Array of search parameters based on property name and property value
    ///   - sortKey: If sort is needed, string that will sort results
    ///   - ascending: Bool determining the ascending order
    /// - Returns: Array containing search results
    func getAllByParameters(parameters: [(key: String, value: Any)], sortKey: String?, ascending: Bool) -> [T] {
        // Temp array holding results
        var allObjects = [T]()
        // Initializing fetch request
        let fetchRequest = NSFetchRequest<T>(entityName: String(describing: T.self))
        // Temp array holding all of the predicates for the fetch request
        var predicates = [NSPredicate]()

        // Iterating all of the search parameters
        parameters.forEach { parameter in

            var tempPredicates = [NSPredicate]()

            // Adding predicates based on value being parsable to different types.
            if let value = parameter.value as? Int32 {
                // Adding predicate to search for int32 value.
                tempPredicates.append(.getIntPredicate(key: parameter.key, value: value))

                // Adding predicate to search for the string value of the int value.
                let stringValue = String(value)
                tempPredicates.append(.getStringPredicate(key: parameter.key, value: stringValue))
            }

            if let value = parameter.value as? String {

                // If the string could be converted to int32, adding predicate based on that int value.
                if let stringValue = Int32(value) {
                    tempPredicates.append(.getIntPredicate(key: parameter.key, value: stringValue))
                }

                tempPredicates.append(.getStringPredicate(key: parameter.key, value: value))
            }

            let compoundPredicate = NSCompoundPredicate(type: .or, subpredicates: tempPredicates)
            predicates.append(compoundPredicate)
        }

        // Adding all of the additional predicates to the main fetch request
        fetchRequest.predicate = NSCompoundPredicate(type: .or, subpredicates: predicates)
        
        // Adding sort descriptor if available
        if let passedSortKey = sortKey {
            let sort = NSSortDescriptor(key: passedSortKey, ascending: ascending)
            fetchRequest.sortDescriptors = [sort]
        }
        
        do {
            // Actually performing fetch
            if let currentContext = context {
                allObjects = try currentContext.fetch(fetchRequest)
            }
            
        } catch let error as NSError {
            print("Fetching of \(String(describing: self)) failed. Error: \(error)")
        }
        
        return allObjects
    }
    
    /// Fetching core data object based on object ID
    /// - Parameter objectId: Unique core data object Id that will be used for fetch
    /// - Returns: Returns core data object matching object id
    func getByObjectId(objectId: NSManagedObjectID) -> T? {
        // Fetching all objects
        let allObjects = getAll(sortKey: nil)

        // Getting first instance of object matching ID
        let matchingObject = allObjects.first { object -> Bool in
            return object.objectID.uriRepresentation() == objectId.uriRepresentation()
        }

        return matchingObject
    }
    
    /// Performing bath deletion of all managed objects
    func deleteAll() {
        guard let fetchRequest = NSFetchRequest<T>(entityName: String(describing: T.self)) as? NSFetchRequest<NSFetchRequestResult> else { return }

        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do {
            if let currentContext = context {
                try currentContext.persistentStoreCoordinator?.execute(deleteRequest, with: currentContext)
            }
            
        } catch let error as NSError {
            print("Deleting all records for \(String(describing: T.self)) failed. Error: \(error)")
        }
    }
}
