//
//  Constants.swift
//  nycschools
//
//  Created by Ismael Zavala on 5/18/22.
//

import UIKit

/// Class holding constants used throughout the project.
class Constants {
    class CustomViewNames {
        static let schoolCellName = "SchoolCell"
    }
    
    class CustomViewProperties {
        static let schoolCellHeight: CGFloat = 160
    }
}
