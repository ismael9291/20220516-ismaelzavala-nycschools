//
//  SchoolPropertyCell.swift
//  nycschools
//
//  Created by Ismael Zavala on 5/18/22.
//

import UIKit
import CoreData

// Generic cell that will be used to show a single property of the school
class SchoolPropertyCell: SchoolDetailsCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    override func configure(name: String?, value: String?) {
        nameLabel.text = name
        valueLabel.text = value
    }
}
