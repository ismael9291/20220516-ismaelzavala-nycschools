//
//  School+CoreDataClass.swift
//  nycschools
//
//  Created by Ismael Zavala on 5/17/22.
//
//

import Foundation
import CoreData

@objc(School)
public class School: NSManagedObject, Codable {
    
    enum SchoolCodingKeys: String, CodingKey {
        case school_name
        case overview_paragraph
        case neighborhood
        case location
        case phone_number
        case fax_number
        case school_email
        case website
        case subway
        case bus
        case finalgrades
        case total_students
        case school_sports
        case graduation_rate
        case attendance_rate
        case primary_address_line_1
        case city
        case zip
        case state_code
        case latitude
        case longitude
        case bbl
        case nta
        case dbn
    }
    
    // Decoding data into score properties
    required convenience public init(from decoder: Decoder) throws {
        
        // Make a managed object context available within init(from:) so we can properly initialize the managed object.
        guard let context = CoreDataManager.sharedManager.persistentContainer?.viewContext else {
            throw NSError()
        }
        
        self.init(context: context)
        
        // Initializing container based on keys that will be used to decode
        let container = try decoder.container(keyedBy: SchoolCodingKeys.self)
        
        do {
            // Attempting to decode individual properties
            self.dbn = try container.decodeIfPresent(String.self, forKey: .dbn)
            self.school_name = try container.decodeIfPresent(String.self, forKey: .school_name)
            self.overview_paragraph = try container.decodeIfPresent(String.self, forKey: .overview_paragraph)
            self.neighborhood = try container.decodeIfPresent(String.self, forKey: .neighborhood)
            self.location = try container.decodeIfPresent(String.self, forKey: .location)
            self.phone_number = try container.decodeIfPresent(String.self, forKey: .phone_number)
            self.fax_number = try container.decodeIfPresent(String.self, forKey: .fax_number)
            self.school_email = try container.decodeIfPresent(String.self, forKey: .school_email)
            self.website = try container.decodeIfPresent(String.self, forKey: .website)
            self.subway = try container.decodeIfPresent(String.self, forKey: .subway)
            self.bus = try container.decodeIfPresent(String.self, forKey: .bus)
            self.finalgrades = try container.decodeIfPresent(String.self, forKey: .finalgrades)
            self.total_students = try container.decodeIfPresent(String.self, forKey: .total_students)
            self.school_sports = try container.decodeIfPresent(String.self, forKey: .school_sports)
            self.graduation_rate = try container.decodeIfPresent(String.self, forKey: .graduation_rate)
            self.attendance_rate = try container.decodeIfPresent(String.self, forKey: .attendance_rate)
            self.primary_address_line_1 = try container.decodeIfPresent(String.self, forKey: .primary_address_line_1)
            self.city = try container.decodeIfPresent(String.self, forKey: .city)
            self.zip = try container.decodeIfPresent(String.self, forKey: .zip)
            self.state_code = try container.decodeIfPresent(String.self, forKey: .state_code)
            self.latitude = try container.decodeIfPresent(String.self, forKey: .latitude)
            self.longitude = try container.decodeIfPresent(String.self, forKey: .longitude)
            self.bbl = try container.decodeIfPresent(String.self, forKey: .bbl)
            self.nta = try container.decodeIfPresent(String.self, forKey: .nta)
            self.dbn = try container.decodeIfPresent(String.self, forKey: .dbn)
            
        } catch let error as NSError {
            print("Error: \(error). Error description: \(error.debugDescription)")
        }
    }
    
    /// Required for Encodable support
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: SchoolCodingKeys.self)
        try container.encode(dbn, forKey: .dbn)
        try container.encode(school_name, forKey: .school_name)
        try container.encode(overview_paragraph, forKey: .overview_paragraph)
        try container.encode(neighborhood, forKey: .neighborhood)
        try container.encode(location, forKey: .location)
        try container.encode(phone_number, forKey: .phone_number)
        try container.encode(fax_number, forKey: .fax_number)
        try container.encode(school_email, forKey: .school_email)
        try container.encode(website, forKey: .website)
        try container.encode(subway, forKey: .subway)
        try container.encode(bus, forKey: .bus)
        try container.encode(finalgrades, forKey: .finalgrades)
        try container.encode(total_students, forKey: .total_students)
        try container.encode(school_sports, forKey: .school_sports)
        try container.encode(graduation_rate, forKey: .graduation_rate)
        try container.encode(attendance_rate, forKey: .attendance_rate)
        try container.encode(primary_address_line_1, forKey: .primary_address_line_1)
        try container.encode(city, forKey: .city)
        try container.encode(zip, forKey: .zip)
        try container.encode(state_code, forKey: .state_code)
        try container.encode(latitude, forKey: .latitude)
        try container.encode(longitude, forKey: .longitude)
        try container.encode(bbl, forKey: .bbl)
        try container.encode(nta, forKey: .nta)
        try container.encode(dbn, forKey: .dbn)
    }
}
