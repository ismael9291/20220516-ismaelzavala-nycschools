//
//  SchoolHelper.swift
//  nycschools
//
//  Created by Ismael Zavala on 5/18/22.
//

import Foundation
import CoreData

/// Protocol that informs when fetching latest schools is complete.
protocol SchoolHelperDelegate: AnyObject {
    func fetchedLatestSchools()
}

/// Helper class in charge of refreshing the latest school results
class SchoolHelper {
    // Data layer to access core data elements.
    let schoolLayer = DataLayer<School>()
    
    // Delegate interested in helper events.
    weak var delegate: SchoolHelperDelegate?
    
    // Any possible search the user has entered.
    var searchString = ""
    
    /// Checking if there are any saved results, fetching data if not.
    func fetchLatestSchoolsIfNeeded() {
        
        // Ensuring count is greater than 0.
        guard schoolLayer.getAll(sortKey: nil).count == 0 else {
            // Results are already saved in core data, no need to fetch again.
            delegate?.fetchedLatestSchools()
            return
        }
        
        // Fetching schools from backend.
        fetchLatestSchools()
    }
    
    /// Clearing saved schools and fetching latest schools from backend.
    func fetchLatestSchools() {
        // Initializing and deleting all schools saved in the device.
        let schoolLayer = DataLayer<School>()
        schoolLayer.deleteAll()
        
        // Initializing and calling service that fetches schools from backend
        let schoolService = SchoolService()
        schoolService.getAllSchools { _ in
            DispatchQueue.main.async {
                // Informing delegate of completion of api call.
                self.delegate?.fetchedLatestSchools()
            }
        }
    }
    
    // Returning total school count in core data.
    func getSchoolCount() -> Int {
        // Getting school count based on search parameter
        let schoolCount = searchString.isEmpty ?
            schoolLayer.getAll(sortKey: nil).count :
            schoolLayer.getAllByParameters(parameters: [(key: "school_name", value: searchString),
                                                        (key: "phone_number", value: searchString),
                                                        (key: "neighborhood", value: searchString)],
                                           sortKey: "school_name", ascending: true).count
        
        return schoolCount
    }
    
    /// Returning school object based on index.
    /// - Parameter index: index of the cell for the school
    /// - Returns: Optional school based on core data query
    func getSchool(index: Int) -> School? {
        // Getting school based on search parameter
        let school = searchString.isEmpty ?
            schoolLayer.getAll(sortKey: "school_name", ascending: true)[safe: index] :
            schoolLayer.getAllByParameters(parameters: [(key: "school_name", value: searchString),
                                                        (key: "phone_number", value: searchString),
                                                        (key: "neighborhood", value: searchString)],
                                           sortKey: "school_name", ascending: true)[safe: index]
        
        return school
    }
    
    /// Returning school object based on object id
    /// - Parameter objectId: object id of school to be fetched
    /// - Returns: Optional school based on core data results.
    func getSchool(objectId: NSManagedObjectID) -> School? {
        return schoolLayer.getByObjectId(objectId: objectId)
    }
}
