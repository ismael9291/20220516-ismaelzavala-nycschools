//
//  SchoolCell.swift
//  nycschools
//
//  Created by Ismael Zavala on 5/17/22.
//

import UIKit
import CoreData

// Cell that will be used to display the school within the home view controller.
class SchoolCell: UITableViewCell {
    
    @IBOutlet weak var schoolValueTextView: UITextView!
    @IBOutlet weak var neighborhoodValueLabel: UILabel!
    @IBOutlet weak var phoneNumberValueLabel: UILabel!
    
    let schoolLayer = DataLayer<School>()
    
    // Setting up cell UI
    func configure(objectId: NSManagedObjectID) {
        // Fetching school
        let school = schoolLayer.getByObjectId(objectId: objectId)
        
        // Setting textview to avoid cutting off school name
        schoolValueTextView.textContainer.lineBreakMode = .byCharWrapping
        
        // Populating labels with school information
        schoolValueTextView.text = school?.school_name ?? "N/A"
        neighborhoodValueLabel.text = school?.neighborhood ?? "N/A"
        phoneNumberValueLabel.text = school?.phone_number ?? "N/A"
    }
}
