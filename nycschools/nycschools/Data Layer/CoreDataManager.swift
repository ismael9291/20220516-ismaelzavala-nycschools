//
//  CoreDataManager.swift
//  nycschools
//
//  Created by Ismael Zavala on 5/17/22.
//

import Foundation
import CoreData

/// Class in charge of loading and saving core data store.
class CoreDataManager {
    // Singleton
    static let sharedManager = CoreDataManager()
    
    let container = NSPersistentContainer(name: "NYCschools")
    var persistentContainer: NSPersistentContainer?
    
    /// Loading core data store and keeping track of container.
    func loadStore() {
        container.loadPersistentStores(completionHandler: { (_, error) in
            if let error = error as NSError? {
                print("Unresolved error \(error), \(error.userInfo)")
            }
        })

        self.persistentContainer = container
    }

    /// Checking if there are any changes to database, saving if so
    func saveContext () {
        let context = CoreDataManager.sharedManager.persistentContainer?.viewContext
        if let currentContext = context, currentContext.hasChanges {
            do {
                try context?.save()
            } catch {
                let error = error as NSError
                print("Unresolved error \(error), \(error.userInfo)")
            }
        }
    }
}
