//
//  HomeScreenVCTests.swift
//  nycschoolsTests
//
//  Created by Ismael Zavala on 5/19/22.
//

import XCTest
import OHHTTPStubs
@testable import nycschools

/// Test for the home screen.
class HomeScreenVCTests: XCTestCase {
    
    var viewController: HomeScreenVC!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // Seting up stubs to ensure successful api call using test json
        setupStub(statusCode: 200)
        
        // Initializing view controller
        viewController = HomeScreenVC(nibName: "HomeScreenVC", bundle: nil)
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        HTTPStubs.removeAllStubs()
    }
    
    func startViewController() {
        viewController.loadView()
        viewController.viewDidLoad()
    }

    /// Testing initial launch of home screen vc
    func testInitialSetup() {
        
        // Ensuring there's no previous core data objects
        let schoolDataLayer = DataLayer<School>()
        schoolDataLayer.deleteAll()
        
        // Creating test school
        let testSchool = schoolDataLayer.create()
        testSchool.school_name = "Testing"
        testSchool.neighborhood = "Test Neighborhood"
        testSchool.phone_number = "123"
        
        // Loading VC
        startViewController()
        
        // Ensuring there's a single school
        XCTAssertEqual(viewController.schoolHelper.getSchoolCount(), 1)
        
        // Ensuring there's one cell
        guard let firstCell = viewController.tableView(viewController.tableView, cellForRowAt: IndexPath(item: 0, section: 0)) as? SchoolCell else {
            XCTFail("Failed to cast cell to school cell.")
            return
        }
        
        // Verifying contents of first cell
        XCTAssertEqual(firstCell.schoolValueTextView.text, "Testing")
        XCTAssertEqual(firstCell.phoneNumberValueLabel.text, "123")
        XCTAssertEqual(firstCell.neighborhoodValueLabel.text, "Test Neighborhood")
    }

    /// Testing school filtering based on search text
    func testSchoolFiltering() throws {
        // Loading view controller
        startViewController()
        
        // Setting new search string in school helper.
        viewController.searchBar(UISearchBar(), textDidChange: "Test")
        
        // Ensuring there are 440 cells in the tableview based on the test json
        XCTAssertEqual(viewController.schoolHelper.searchString, "Test")
    }
    
    // MARK: - stub
    private func setupStub(statusCode: Int32) {
        let filePath = "schools.json"
        stub(condition: isHost("data.cityofnewyork"), response: { _ in
            guard let fileAtPath = OHPathForFile(filePath, type(of: self)) else { return HTTPStubsResponse() }

            return HTTPStubsResponse(
                fileAtPath: fileAtPath,
                statusCode: statusCode,
                headers: ["Content-Type": "application/json"]
            )
        })
    }
}
