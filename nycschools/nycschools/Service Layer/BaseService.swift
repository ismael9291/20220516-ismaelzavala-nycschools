//
//  BaseService.swift
//  nycschools
//
//  Created by Ismael Zavala on 5/17/22.
//

import Foundation

typealias ServiceCompletionHandler = ((Any) -> Void)

/// Base class that will have global settings for all api calls
class BaseService {
    
    /// Getting data using url string
    /// - Parameters:
    ///   - requestUrl: Url string that will be used to perform api call
    ///   - serviceCompletion: Completion block called when api call completes
    func performApiCall(requestUrl: String, serviceCompletion: @escaping ServiceCompletionHandler) {
        // Converting string to URL
        guard let url = URL(string: requestUrl) else {
            serviceCompletion(false)
            return
        }
        
        // Creating task using url.
        let urlTask = URLSession.shared.dataTask(with: url) { data, response, error in
            // Calling function that each service should override
            self.callCompleted(data: data, response: response, error: error, serviceCompletion: serviceCompletion)
        }
        
        // Starting call
        urlTask.resume()
    }
    
    // Should be overriden by individual services
    func callCompleted(data: Data?, response: URLResponse?, error: Error?, serviceCompletion: ServiceCompletionHandler) {
        fatalError("Each individual service call should override this function")
    }
}
