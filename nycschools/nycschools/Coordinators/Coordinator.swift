//
//  Coordinator.swift
//  nycschools
//
//  Created by Ismael Zavala on 5/16/22.
//

import UIKit

/// Protocol used by all coordinators to ensure all contain navigation controller and start functionality
protocol Coordinator {
    var rootNav: UINavigationController? {get set}
    func start()
}
