//
//  SchoolService.swift
//  nycschools
//
//  Created by Ismael Zavala on 5/18/22.
//

import Foundation

class SchoolService: BaseService {
    
    /// Function that gets called when base service completes performing its api call.
    /// - Parameters:
    ///   - data: Data received from api
    ///   - response: Response received from the api
    ///   - error: Any potential error when performing api call
    ///   - serviceCompletion: Completion block passed from this service
    override func callCompleted(data: Data?, response: URLResponse?, error: Error?, serviceCompletion: (Any) -> Void) {
        // Ensuring completion block is called when complete.
        defer {
            serviceCompletion(false)
        }
        
        // Ensuring there's data received from api
        guard let fetchedData = data else {
            return
        }
        
        do {
            // Initializing coder, attempting to decode data into school objects.
            let decoder = JSONDecoder()
            let _ = try decoder.decode([School].self, from: fetchedData)
            
        } catch let error as NSError {
            print("Error performing score api: \(error.localizedDescription)")
        }
    }
    
    /// Performing api call to fetch scores of NYC schools
    /// - Parameter completion: Completion block that will eventually be called when call completes
    func getAllSchools(completion: @escaping  ServiceCompletionHandler) {
        let schoolsUrlString = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
        
        // Using base service function to perform api call
        performApiCall(requestUrl: schoolsUrlString, serviceCompletion: completion)
    }
}
